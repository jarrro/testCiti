var gulp = require('gulp');
var sass = require('gulp-sass');
var minCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');

gulp.task('sass', function() {
    // console.log(10)
    gulp.src('./node_modules/bootstrap/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css'))
        .pipe(minCss())
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('style', function() {
    gulp.src('./assets/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css'))
        .pipe(minCss())
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch', function() {
    livereload.listen();
    gulp.watch('./node_modules/bootstrap/scss/**/*.scss', ['sass']).on('change', livereload.changed);
    gulp.watch('./assets/css/*.scss', ['style']).on('change', livereload.changed);

    gulp.watch('index.html').on('change', livereload.changed);
});