(function() {
    'use strict';

    window.addEventListener('load', function() {
        var items = document.getElementsByClassName('item');
        
        [].forEach.call(items, function(el) {
            el.addEventListener('mouseover', function() {
                el.getElementsByClassName('item-body')[0].style.top = "0";
            });
            el.addEventListener('mouseout', function() {
                el.getElementsByClassName('item-body')[0].style.top = "100px";
            })
        });
    }); 
})();