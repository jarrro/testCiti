(function() {
    "use strict";
    window.addEventListener("load", function() {
      var form = document.getElementById("needs-validation");
      form.addEventListener("submit", function(event) {
        if (form.checkValidity() == false) {
          event.preventDefault();
          event.stopPropagation();
          $('input').tooltip('hide');
        }
        form.classList.add("was-validated");
        $('input').tooltip('show');
      }, false);
    }, false);
  }());